// import * as React from 'react';
import React, { useState } from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';

import './FormList.css'
import { useNavigate } from 'react-router-dom';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#FBB03B',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));


function FormList() {

    const navigate = useNavigate();

    const handleAddData = () => {
        navigate('/Form')
    }

    let Data = JSON.parse(localStorage.getItem('Form'));
    const [forms, setForms] = useState(Data);


    const handleDelete = (formIndex) => {
        Data.map((form, Index) => {
            if (Index === formIndex) {
                const formList = [...Data];
                formList.splice(formIndex, 1);
                const newList = formList;
                setForms(newList);
                console.log(newList)
                localStorage.setItem('Form', JSON.stringify(newList))
                return newList
            }
        })
    }

    const handleEdit = (formIndex) => {
        navigate(`/Form/${formIndex}`)
    }
    
    return (
        <React.Fragment>
            <CssBaseline />
            <Container fixed>
                <Box sx={{ bgcolor: '#cfe8fc', height: '100vh' }} display="flex" flexDirection="column">
                    <Box className='btn-add'>
                        <Button variant="contained" color="success" sx={{ height: 40 }} onClick={handleAddData}>
                            Add Data
                        </Button>
                    </Box>
                    <br></br>
                    
                   {(Data !== null && Data !== undefined ) ? (Data.map((form, formIndex) => {
                        // console.log(form)
                        return (
                            <Box sx={{ width: '100%' }} key={formIndex} >
                                <Stack spacing={2} >
                                    <Item className='quiz-item'> {form.name.value}
                                        <Button variant="outlined" startIcon={<DeleteIcon />} onClick={() => { handleDelete(formIndex) }} >
                                            Delete
                                        </Button>
                                        <Button variant="outlined" startIcon={<EditIcon />} onClick={() => { handleEdit(formIndex) }} >
                                            EDIT
                                        </Button>
                                    </Item>
                                </Stack>
                            </Box>) 
                    })) : (<></>)}
                </Box>
            </Container>
        </React.Fragment>
    );
}
export default FormList