import React, { useState, useEffect } from 'react'
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormLabel from '@mui/material/FormLabel';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { Stack } from '@mui/system';
import Button from '@mui/material/Button';
import { Female, SnippetFolderSharp } from '@mui/icons-material';
// import FormHelperText from '@mui/material/FormHelperText';

import { useNavigate, useParams } from 'react-router-dom';

// const formData = {
//     course: "",
//     firstName: "",
//     lastName: "",
//     gender: Female,
//     email: "",
//     date: 0,
//     data: ""
// }

function Form(props) {

    const { type } = props;
    const { index } = useParams();
    const navigate = useNavigate()
    // const [data, setData] = useState(formData);
    // console.log(data)

    const [formValues, setFormValues] = useState({
        salutation: {
            value: '',
            error: false,
            errorMessage: 'This field mush not be empty'
        },
        name: {
            value: '',
            error: false,
            errorMessage: 'You must enter your First Name'
        },
        lname: {
            value: '',
            error: false,
            errorMessage: 'You must enter your Last Name'
        },
        gender: {
            value: '',
            error: null,
            errorMessage: 'You must enter your Gender'
        },
        email: {
            value: '',
            error: null,
            errorMessage: 'You must enter your MailID'
        },
        dob: {
            value: '',
            error: null,
            errorMessage: 'You must choose your Date of Birth'
        },
        details: {
            value: '',
            error: null,
            errorMessage: 'You must choose your Date of Birth'
        }
    })

    useEffect(() => {
        let formList = JSON.parse(localStorage.getItem('Form'));
        // if (type === 'edit') {
        //     const notUpdatedValue = { ...formValues };
        //     notUpdatedValue.name.value = 'sfsdf';
        //     setFormValues(notUpdatedValue);
        // }
        // let copyData = { ...data };
        if (type === 'edit') {
            // { console.log("checking---1", formValues, formValues.salutation.value) }
            const updatedValue = { ...formValues };
            console.log(updatedValue)
            const filteredData = formList.filter((item, i) => parseInt(index) === i)
            updatedValue.salutation.value = filteredData[0].salutation.value
            updatedValue.name.value = filteredData[0].name.value
            updatedValue.lname.value = filteredData[0].lname.value
            updatedValue.gender.value = filteredData[0].gender.value
            updatedValue.email.value = filteredData[0].email.value
            updatedValue.dob.value = filteredData[0].dob.value
            updatedValue.details.value = filteredData[0].details.value

            // console.log({ copyData, filteredData })
            setFormValues(updatedValue)
        }
    }, [])



    const handleSubmit = () => {
        // console.log("entered handle submit");
        const existingData = JSON.parse(localStorage.getItem('Form'));

        if (existingData !== null) {
            if (type === 'add') {
                console.log("existingarray", existingData)
                existingData.push(formValues);
                console.log("newarray", existingData)
                localStorage.setItem('Form', JSON.stringify(existingData));
                navigate('/');
            } else if (type === 'edit') {
                console.log("entered edit")
                const existingEditObj = existingData[index];
                existingEditObj.salutation.value = formValues.salutation.value;
                existingEditObj.name.value = formValues.name.value;
                existingEditObj.lname.value = formValues.lname.value;
                existingEditObj.gender.value = formValues.gender.value;
                existingEditObj.email.value = formValues.email.value;
                existingEditObj.dob.value = formValues.dob.value;
                existingEditObj.details.value = formValues.details.value;
                localStorage.setItem('Form', JSON.stringify(existingData));
                navigate('/');
            }

        } else {
            const array = []
            array.push(formValues);
            localStorage.setItem('Form', JSON.stringify(array));
            navigate('/');
        }
    }


    // console.log(formValues)

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({
            ...formValues,
            [name]: {
                ...formValues[name],
                value
            }
        })
    }

    const handleValidate = (e) => {

        const formFields = Object.keys(formValues);
        let newFormValues = { ...formValues }
        let noError = true
        for (let index = 0; index < formFields.length; index++) {
            const currentField = formFields[index];
            const currentValue = formValues[currentField].value;

            if (currentValue === '') {
                noError = false
                newFormValues = {
                    ...newFormValues,
                    [currentField]: {
                        ...newFormValues[currentField],
                        error: true
                    }
                }
            }
        }

        setFormValues(newFormValues)
        if(noError)
        handleSubmit()
    }


    return (
        <React.Fragment>
            {/* {console.log("checking---2", formValues, formValues.salutation.value)} */}
            <CssBaseline />
            <Container maxWidth="sm">
                <Box sx={{ bgcolor: '#cfe8fc', height: '100vh', display: 'flex', flexDirection: 'column' }}>
                    <Stack spacing={2}>
                        {/* SELECT DROP DOWN */}
                        <FormControl required sx={{ m: 1, minWidth: 120 }}>
                            <InputLabel id="demo-simple-select-required-label">Salutation</InputLabel>
                            <Select
                                labelId="demo-simple-select-required-label"
                                id="demo-simple-select-required"
                                label="Salutation"
                                name="salutation"
                                value={formValues.salutation.value}
                                onChange={handleChange}
                                error={formValues.salutation.error}
                                helperText={formValues.salutation.error && formValues.salutation.errorMessage}
                                required
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                <MenuItem value={"Mr."}>Mr.</MenuItem>
                                <MenuItem value={"Ms."}>Ms.</MenuItem>
                                <MenuItem value={"Mrs."}>Mrs.</MenuItem>
                            </Select>
                            <FormHelperText></FormHelperText>
                        </FormControl>
                        {/* FIRST NAME */}
                        <TextField
                            required
                            id="outlined-required"
                            label="First Name"
                            name="name"
                            // className={classes.field}
                            value={formValues.name.value}
                            onChange={handleChange}
                            error={formValues.name.error}
                            helperText={formValues.name.error && formValues.name.errorMessage}
                        />
                        {/* LAST NAME */}
                        <TextField
                            required
                            id="outlined-required"
                            label="Last Name"
                            name="lname"
                            // className={classes.field}
                            value={formValues.lname.value}
                            onChange={handleChange}
                            error={formValues.lname.error}
                            helperText={formValues.lname.error && formValues.lname.errorMessage}
                        // defaultValue="Hello World"
                        />
                        {/* GENDER */}
                        <FormControl >
                            <FormLabel id="demo-controlled-radio-buttons-group">Gender</FormLabel>
                            <RadioGroup
                                aria-labelledby="demo-controlled-radio-buttons-group"
                                name="gender"
                                value={formValues.gender.value}
                                onChange={handleChange}
                                required
                            >
                                <FormControlLabel value="female" control={<Radio />} label="Female" />
                                <FormControlLabel value="male" control={<Radio />} label="Male" />
                            </RadioGroup>
                        </FormControl>
                        {/* EMAIL  */}
                        <TextField
                            required
                            id="outlined-required"
                            label="Email"
                            name="email"
                            onChange={handleChange}
                            // className={classes.field}
                            value={formValues.email.value}

                            error={formValues.email.error}
                            helperText={formValues.email.error && formValues.email.errorMessage}
                        />
                        {/* DOB */}
                        <LocalizationProvider dateAdapter={AdapterDayjs} >
                            {/* <DatePicker
                                required
                                label="DOB"
                                name="dob"
                                value={formValues.dob.value}
                                onChange={handleChange}
                                renderInput={(params) => <TextField {...params} />}
                            /> */}
                            <TextField
                                required
                                id="date"
                                label="DOB"
                                name="dob"
                                type="date"
                                value={formValues.dob.value}
                                onChange={handleChange}
                                defaultValue="2017-05-24"
                                sx={{ width: 220 }}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </LocalizationProvider>
                        {/* ADDRESS */}
                        <TextField
                            required
                            id="outlined-multiline-static"
                            label="Address"
                            multiline
                            rows={4}
                            name="details"
                            // className={classes.field}
                            value={formValues.details.value}
                            onChange={handleChange}
                            error={formValues.details.error}
                            helperText={formValues.details.error && formValues.details.errorMessage}
                        // defaultValue="Default Value"
                        />
                        {/* SUBMIT BUTTON */}
                        <Button variant="contained" color="success" onClick={() => handleValidate()}>
                            Submit
                        </Button>
                    </Stack>
                </Box>
            </Container>
        </React.Fragment>
    );
}
export default Form