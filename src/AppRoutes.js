import React, { lazy } from 'react';
import { Routes, Route } from 'react-router-dom';

const FormList = lazy(()=>import('C:/tasks/Form with Validation/form/src/components/FormList/FormList.js'));
const Form = lazy(()=>import('C:/tasks/Form with Validation/form/src/components/Form/Form.js'));

function AppRoutes(){
    return (
        <Routes>
            <Route path='/' exact element={<FormList />} />
            <Route path='/Form' exact element={<Form type="add" />} />
            <Route path='/Form/:index' exact element={<Form type="edit" />} />
        </Routes>
    );
}

export default AppRoutes;
