import React, { Suspense, lazy } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';

const AppRoute = lazy(() => import('C:/tasks/Form with Validation/form/src/AppRoutes.js'));

function App() {
  // useEffect(() => {
  //   localStorage.setItem('LoginData', JSON.stringify(records));
  // }, [])
  return (
    <BrowserRouter>
      <Suspense fallback={<div>Loading...</div>}>
        <Routes>
          <Route path='*' element={<AppRoute />} />
        </Routes>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
